# MeFit Case (.NET + React)

Solution for managing weekly workout goals & motivate users to complete the exercise regime.



## Team members:

- Akisan
- Camilla
- Dawit
- Nicole



## How to run:

We have made our solution using .NET with ASP.NET Library and Entity Framework for our backend, while using React with Redux for state management and Material UI for design.

#### Prerequisites:

**Frontend - React Project:**

- Active Internet Connection
- IDE Like **Webstrom** or Visual Studio Code
- **npm & Node.js**

Hosted Solution Also Require:

- **Heroku**
  - Heroku CLI

<br><br>

**Backend - ASP.NET Project:**

- **Visual Studio**

  - With Following "Workloads" installed using the Visual Studio Installer
    - **ASP.NET** and web development
    - .NET desktop development
    - Data storage and processing 

  - .NET 5 Runtime or Above 
  - .NET Framework 4.8 SDK or Above
  - .NET SDK

- **Microsoft SQL Server 2019** Developer

  - SQL Server Management Studio

Hosted Solution Also Require:

- **Docker**
  - Docker Desktop
  - Docker CLI
  - Docker Hub
- **Azure**
  - **SQL Server**
    - **SQL Database**
  - App Service Plan
  - **App Service** ( Host Website )

<br><br>

**Version Control: GitLab**

<br>

<br>

#### Frontend:

Before proceeding make sure the backend is up and running. Currently the code points the backend project that hosted online, if you wish to run the entire solution locally you will have change all of the API URLs to point to https://localhost:44301/ rather then https://mefit.azurewebsites.net/ which it currently points to.

Assuming that you only want to run the frontend locally with the backend project hosted online, do following:

Once you have cloned the frontend repository: [nicole bendu / MeFit · GitLab](https://gitlab.com/nicben/mefit)

All you will need to do is to navigate to the root directory of the project and enter `npm start` in the terminal, this will run the project locally on you machine.

#### Backend:

Start by opening the project in Visual Studio by double clicking the .sln file that looks like this: ![image-20211026132826594](https://gitlab.com/nicben/mefit/-/raw/master/images/image-20211026132826594.png)

Now that you got the solution up and running in Visual Studio it might take some time to download additional need resources, just give it some time while it sets up everything. A quick way to tell if it's done is by looking at the bottom left corner where there is a indicator telling you whether it is done, when its done it will look something like this: ![image-20211026133217890](https://gitlab.com/nicben/mefit/-/raw/master/images/image-20211026133217890.png)

To run the application successfully you will need to create the database first. Now assuming that you want to run the SQL Server locally as well, this would be a nice time to change the connectionString to point to the local database. This is done in the Startup.cs file by finding line 49: 

```c#
options.UseSqlServer(Configuration.GetConnectionString("ConnectionString")));
```

and swapping this with the information needed to connect to the local SQL Server, for this it will be helpful to run the SQL Server Management Studio when the program starts up you will be presented with a connection screen like this: ![image-20211026140015873](https://gitlab.com/nicben/mefit/-/raw/master/images/image-20211026140015873.png)

Take note of the Server name as we need that for the connectionString.

![image-20211026140131834](https://gitlab.com/nicben/mefit/-/raw/master/images/image-20211026140131834.png)

Back to  line 49 in Startup.cs replace this 

```c#
options.UseSqlServer(Configuration.GetConnectionString("ConnectionString")));
```

with this, remember to replace the entire "[SERVER_NAME]" part with the Server name you got from the SQL Server Connection Screen:

```c#
options.UseSqlServer("Data Source=[SERVER_NAME];Initial Catalog=MeFitDB;Integrated Security=True;"));
```



Now we can focus on creating the database structure. This is done by creating a migration by entering following in the terminal: `add-migration initLocalDB`. The terminal can be found by Pressing Tools in the Toolbar on Top of the screen, then NuGet Package Manager, then Package Manager Console.

Once its done creating the migration we need to update the database with this command: `dotnet ef database update`



Now that the database is created we can run the project by pressing the Play button marked as IIS Express: ![image-20211026133744233](https://gitlab.com/nicben/mefit/-/raw/master/images/image-20211026133744233.png)
in the toolbar


