﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MeFit_API.Migrations
{
    public partial class initAzure : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Account",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FitnessLevel = table.Column<int>(type: "int", nullable: false),
                    ImageUrl = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Height = table.Column<int>(type: "int", nullable: false),
                    Weight = table.Column<int>(type: "int", nullable: false),
                    Admin = table.Column<int>(type: "int", nullable: false),
                    Contributer = table.Column<int>(type: "int", nullable: false),
                    FirstName = table.Column<string>(type: "nvarchar(26)", maxLength: 26, nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(26)", maxLength: 26, nullable: false),
                    Email = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    GoogleId = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Account", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Exercise",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    VideoUrl = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Muscules = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Exercise", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Program",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Category = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Program", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Workout",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Type = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Difficulty = table.Column<int>(type: "int", nullable: false),
                    Completed = table.Column<int>(type: "int", nullable: false),
                    MadeByContributor = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Workout", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Goal",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    StartDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Completed = table.Column<bool>(type: "bit", nullable: false),
                    Difficulty = table.Column<int>(type: "int", nullable: false),
                    AccountId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Goal", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Goal_Account_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Account",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Set",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Reps = table.Column<int>(type: "int", nullable: false),
                    Sets = table.Column<int>(type: "int", nullable: false),
                    ExerciseId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Set", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Set_Exercise_ExerciseId",
                        column: x => x.ExerciseId,
                        principalTable: "Exercise",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProgramWorkouts",
                columns: table => new
                {
                    ProgramId = table.Column<int>(type: "int", nullable: false),
                    WorkoutId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProgramWorkouts", x => new { x.ProgramId, x.WorkoutId });
                    table.ForeignKey(
                        name: "FK_ProgramWorkouts_Program_ProgramId",
                        column: x => x.ProgramId,
                        principalTable: "Program",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProgramWorkouts_Workout_WorkoutId",
                        column: x => x.WorkoutId,
                        principalTable: "Workout",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "GoalWorkouts",
                columns: table => new
                {
                    WorkoutId = table.Column<int>(type: "int", nullable: false),
                    GoalId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GoalWorkouts", x => new { x.WorkoutId, x.GoalId });
                    table.ForeignKey(
                        name: "FK_GoalWorkouts_Goal_GoalId",
                        column: x => x.GoalId,
                        principalTable: "Goal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GoalWorkouts_Workout_WorkoutId",
                        column: x => x.WorkoutId,
                        principalTable: "Workout",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SetWorkouts",
                columns: table => new
                {
                    SetId = table.Column<int>(type: "int", nullable: false),
                    WorkoutId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SetWorkouts", x => new { x.SetId, x.WorkoutId });
                    table.ForeignKey(
                        name: "FK_SetWorkouts_Set_SetId",
                        column: x => x.SetId,
                        principalTable: "Set",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SetWorkouts_Workout_WorkoutId",
                        column: x => x.WorkoutId,
                        principalTable: "Workout",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Account",
                columns: new[] { "Id", "Admin", "Contributer", "Email", "FirstName", "FitnessLevel", "GoogleId", "Height", "ImageUrl", "LastName", "Weight" },
                values: new object[,]
                {
                    { 1, 1, 2, "NOHAVE", "Test", 5, "1", 170, "NO_IMAGE", "Account 1", 90 },
                    { 2, 3, 3, "NOHAVE", "Test", 100, "2", 180, "NO_IMAGE", "Account 2", 70 }
                });

            migrationBuilder.InsertData(
                table: "Exercise",
                columns: new[] { "Id", "Description", "Muscules", "Name", "VideoUrl" },
                values: new object[,]
                {
                    { 15, "", "Biceps, Triceps, Quad", "Band Pulls", "" },
                    { 14, "", "Biceps, Triceps", "DB Reverse Flies", "" },
                    { 13, "", "Biceps, Triceps, Gluts", "Seated Row", "" },
                    { 12, "", "Biceps, Triceps, Bench Muscle", "DB Bench Press", "" },
                    { 11, "", "Biceps, Triceps", "Weighted Pull Ups", "" },
                    { 10, "", "Biceps, Triceps", "DB Front Squat", "" },
                    { 9, "", "Legs, Hip", "Single Leg Hip Thrust", "" },
                    { 8, "", "Biceps, Triceps", "Bulgarian Split Squat", "" },
                    { 7, "", "Biceps, Triceps", "Conventional Deadlift", "" },
                    { 6, "", "Biceps, Triceps", "Barbbell Squat", "" },
                    { 5, "", "Biceps, Triceps", "Overhead DB Extensions", "" },
                    { 4, "", "Biceps, Triceps", "DB Lateral Raises", "" },
                    { 3, "The activity of running at a steady, gentle pace as a form of physical exercise", "Glutes, Hamstrings, Quads", "Jogging", "https://www.youtube.com/watch?v=Hv5UvqcsIfY" },
                    { 2, "The action or movement of a runner", "Glutes, Hamstrings, Quads", "Running", "https://www.youtube.com/watch?v=_kGESn8ArrU" },
                    { 1, "A push-up (sometimes called a press-up in British English) is a common calisthenics exercise beginning from the prone position. ", "Triceps, Pectorals (Chest), Shoulders (Deltoids)", "Push Ups", "https://www.youtube.com/watch?v=IODxDxX7oi4" }
                });

            migrationBuilder.InsertData(
                table: "Program",
                columns: new[] { "Id", "Category", "Title" },
                values: new object[,]
                {
                    { 2, "Strength", "Get Thicc Legs" },
                    { 1, "Flexibility", "Workout program for beginners" }
                });

            migrationBuilder.InsertData(
                table: "Workout",
                columns: new[] { "Id", "Completed", "Difficulty", "MadeByContributor", "Title", "Type" },
                values: new object[,]
                {
                    { 1, 0, 5, true, "Leg day", "Legs" },
                    { 2, 1, 3, true, "Arm day", "Arms" },
                    { 3, 0, 2, true, "Graveyard Shift", "Strenght, Tone" }
                });

            migrationBuilder.InsertData(
                table: "Goal",
                columns: new[] { "Id", "AccountId", "Completed", "Difficulty", "StartDate", "Title" },
                values: new object[,]
                {
                    { 1, 1, false, 3, new DateTime(2021, 10, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "Summer Body" },
                    { 2, 1, true, 1, new DateTime(2021, 10, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), "Find Your Max BPM" },
                    { 3, 2, true, 45, new DateTime(2021, 10, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), "Get Stronger" }
                });

            migrationBuilder.InsertData(
                table: "ProgramWorkouts",
                columns: new[] { "ProgramId", "WorkoutId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 1, 2 },
                    { 2, 3 }
                });

            migrationBuilder.InsertData(
                table: "Set",
                columns: new[] { "Id", "ExerciseId", "Reps", "Sets" },
                values: new object[,]
                {
                    { 1, 1, 5, 20 },
                    { 2, 2, 1, 1 }
                });

            migrationBuilder.InsertData(
                table: "GoalWorkouts",
                columns: new[] { "GoalId", "WorkoutId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 1, 2 },
                    { 2, 3 },
                    { 3, 3 }
                });

            migrationBuilder.InsertData(
                table: "SetWorkouts",
                columns: new[] { "SetId", "WorkoutId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 2, 1 },
                    { 2, 2 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Goal_AccountId",
                table: "Goal",
                column: "AccountId");

            migrationBuilder.CreateIndex(
                name: "IX_GoalWorkouts_GoalId",
                table: "GoalWorkouts",
                column: "GoalId");

            migrationBuilder.CreateIndex(
                name: "IX_ProgramWorkouts_WorkoutId",
                table: "ProgramWorkouts",
                column: "WorkoutId");

            migrationBuilder.CreateIndex(
                name: "IX_Set_ExerciseId",
                table: "Set",
                column: "ExerciseId");

            migrationBuilder.CreateIndex(
                name: "IX_SetWorkouts_WorkoutId",
                table: "SetWorkouts",
                column: "WorkoutId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GoalWorkouts");

            migrationBuilder.DropTable(
                name: "ProgramWorkouts");

            migrationBuilder.DropTable(
                name: "SetWorkouts");

            migrationBuilder.DropTable(
                name: "Goal");

            migrationBuilder.DropTable(
                name: "Program");

            migrationBuilder.DropTable(
                name: "Set");

            migrationBuilder.DropTable(
                name: "Workout");

            migrationBuilder.DropTable(
                name: "Account");

            migrationBuilder.DropTable(
                name: "Exercise");
        }
    }
}
