﻿using AutoMapper;
using MeFit_API.Models.DTO.ProfileDTO;
using MeFit_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MeFit_API.Models.DTO.MusculeDTO;

namespace MeFit_API.Mapper
{
    public class MusculeProfile : Profile
    {
        public MusculeProfile()
        {
            CreateMap<Muscule, MusculeReadDTO>();

            // Profile < --- > Profile...DTO
            CreateMap<Muscule, MusculeCreateDTO>()
                .ReverseMap();

            CreateMap<Muscule, MusculeEditDTO>()
                    .ReverseMap();

            // NO???
        }
    }
}
