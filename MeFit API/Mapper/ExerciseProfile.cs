﻿using AutoMapper;
using MeFit_API.Models.DTO.UserDTO;
using MeFit_API.Models.DTO.WorkoutDTO;
using MeFit_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MeFit_API.Models.DTO.ExerciseDTO;

namespace MeFit_API.Mapper
{
    public class ExerciseProfile : Profile
    {
        public ExerciseProfile()
        {
            CreateMap<Exercise, ExerciseReadDTO>();
                 //.ForMember(pdto => pdto.Muscules, opt => opt
                 //.MapFrom(p => p.Muscules.Select(c => c.ToString()).ToArray()));
                 //.ForMember(pdto => pdto.Workouts, opt => opt
                 //.MapFrom(p => p.Workouts.Select(c => c.Id).ToArray()));

            // Profile < --- > Profile...DTO
            CreateMap<Exercise, ExerciseCreateDTO>()
                .ReverseMap();

            CreateMap<Exercise, ExerciseEditDTO>()
                    .ReverseMap();
        }
    }
}
