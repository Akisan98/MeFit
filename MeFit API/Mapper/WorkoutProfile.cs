﻿
using System.Linq;
using AutoMapper;
using MeFit_API.Models;
using MeFit_API.Models.DTO.UserDTO;
using MeFit_API.Models.DTO.WorkoutDTO;
namespace MeFit_API.Mapper
{
    public class WorkoutProfile: Profile
    {
        public WorkoutProfile()
        {
            CreateMap<Workout, WorkoutReadDTO>()
                 .ForMember(pdto => pdto.Sets, opt => opt
                 .MapFrom(p => p.Sets.Select(c => c.Id).ToArray()));
                 //.ForMember(pdto => pdto.Goals, opt => opt
                 //.MapFrom(p => p.Goals.Select(c => c.Id).ToArray()));

            // Profile < --- > Profile...DTO
            CreateMap<Workout, WorkoutCreateDTO>()
                .ReverseMap();

            CreateMap<Workout, WorkoutCreateDTO2>()
                .ReverseMap();

            CreateMap<Workout, WorkoutEditDTO>()
                    .ReverseMap();
        }
    }
}
