﻿using AutoMapper;
using MeFit_API.Models.DTO.UserDTO;
using MeFit_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MeFit_API.Models.DTO.SetDTO;

namespace MeFit_API.Mapper
{
    public class SetProfile : Profile
    {
        public SetProfile()
        {
            CreateMap<Set, SetReadDTO>();

            // Goal < --- > Goal...DTO
            CreateMap<Set, SetCreateDTO>()
                .ReverseMap();

            CreateMap<SetEditDTO, Set>();

            //CreateMap<User, UserEditDTO>()
            //        .ReverseMap();
        }

    }
}
