﻿using AutoMapper;
using MeFit_API.Models.DTO.GoalDTO;
using MeFit_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MeFit_API.Models.DTO.UserDTO;

namespace MeFit_API.Mapper
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserReadDTO>();

            // Goal < --- > Goal...DTO
            CreateMap<User, UserCreateDTO>()
                .ReverseMap();

            CreateMap<UserEditDTO, User>();

            //CreateMap<User, UserEditDTO>()
            //        .ReverseMap();
        }
    }
}
