﻿using AutoMapper;
using System;
using MeFit_API.Models.DTO.GoalDTO;
using System.Collections.Generic;
using Account = MeFit_API.Models.Account;
using System.Linq;
using System.Threading.Tasks;
using MeFit_API.Models;
namespace MeFit_API.Mapper
{
    public class GoalsProfile: Profile
    {
        public GoalsProfile()
        {
            CreateMap<Goal,GoalReadDTO>()
                //.ForMember(pdto => pdto.Accounts, opt => opt
                //.MapFrom(p => p.Accounts.Select(c => c.UserId).ToArray()))
                .ForMember(gdto => gdto.Workouts, opt => opt
                .MapFrom(g => g.Workouts.Select(c => c.Id).ToArray()));

            // Goal < --- > Goal...DTO
            CreateMap<Goal, GoalCreateDTO>()
                .ReverseMap();

            CreateMap<Goal, GoalEditDTO>()
                    .ReverseMap();
        }
    }
}
