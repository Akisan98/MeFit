﻿using AutoMapper;
using MeFit_API.Models;
using MeFit_API.Models.DTO.ProfileDTO;
using System.Linq;

namespace MeFit_API.Profiles
{
    public class AccountProfile : Profile
    {
        public AccountProfile()
        {
            CreateMap<Account, AccountReadDTO>()
                 .ForMember(pdto => pdto.Goals, opt => opt
                 .MapFrom(p => p.Goals.Select(c => c.Id).ToArray()));

            // Profile < --- > Profile...DTO
            CreateMap<Account, AccountCreateDTO>()
                .ReverseMap();

            CreateMap<Account, AccountEditDTO>()
                    .ReverseMap();

            // NO???
        }
    }
}
