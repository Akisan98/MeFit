﻿using AutoMapper;
using MeFit_API.Models.Domain;
using MeFit_API.Models.DTO.TrainingProgramDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;

namespace MeFit_API.Mapper
{
    public class ProgramProfile : Profile
    {
        public ProgramProfile()
        {
            CreateMap<TrainingProgram, TrainingProgramReadDTO>()
                .ForMember(tdto => tdto.Workouts, opt => opt
                .MapFrom(g => g.Workouts.Select(c => c.Id).ToArray()));

            // Goal < --- > Goal...DTO
            CreateMap<TrainingProgram, TrainingProgramCreateDTO>()
                .ReverseMap();

            CreateMap<TrainingProgram, TrainingProgramEditDTO>()
                .ReverseMap();
        }
    }
}
