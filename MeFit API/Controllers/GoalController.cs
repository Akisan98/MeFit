﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net.Mime;
using System.Threading.Tasks;
using MeFit_API.Models;
using AutoMapper;
using MeFit_API.Services.GoalService;
using MeFit_API.Models.DTO.GoalDTO;
using MeFit_API.Models.DTO.ProfileDTO;

namespace MeFit_API.Controllers
{
    [Route("api/v1/goal")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class GoalController : ControllerBase   
    {
        private readonly IMapper _mapper;
        private readonly IGoalService _goalService;

        public GoalController(IMapper mapper, IGoalService goalService)
        {
            _mapper = mapper;
            _goalService = goalService;
        }

        /// <summary>
        /// Gets a specific Goal by their Id
        /// </summary>
        /// <param name="id">The Id of Specified Goal</param>
        /// <returns>A Instance of GoalReadDTO (Goal)</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<GoalReadDTO>> GetGoal(int id)
        {
            Goal goal = await _goalService.GetGoaleById(id);
            if (goal == null)
            {
                return NotFound();
            }
            return _mapper.Map<GoalReadDTO>(goal);
        }
        
        /// <summary>
        /// Gets all Goals in DB
        /// </summary>
        /// <returns>List of Goals</returns>
        [HttpGet()]
        public async Task<ActionResult<IEnumerable<GoalReadDTO>>> GetGoals()
        {
            return _mapper.Map<List<GoalReadDTO>>(await _goalService.GetAllGoals());
        }

        /// <summary>
        /// Creates A new Goal
        /// </summary>
        /// <param name="dtoGoal"></param>
        /// <returns>Instance of created Goal</returns>
        [HttpPost()]
        public async Task<ActionResult<Goal>> AddGoal(GoalCreateDTO dtoGoal)
        {
            Goal domainGoal = _mapper.Map<Goal>(dtoGoal);
            domainGoal = await _goalService.AddGoal(domainGoal);
            return CreatedAtAction("GetGoal", new { id = domainGoal.Id }, _mapper.Map<Goal>(domainGoal));
        }
        
        /// <summary>
        /// Deletes Specifed Goal
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Reponse Code</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult<GoalReadDTO>> RemoveGoal(int id)
        {
            if (!_goalService.GoalExists(id))
            {
                return NotFound();
            }

            await _goalService.RemoveGoalCascading(id);
            return NoContent();
        }

        [HttpDelete("{id}/goalAndWorkout")]
        public async Task<ActionResult<GoalReadDTO>> RemoveGoal2(int id)
        {
            if (!_goalService.GoalExists(id))
            {
                return NotFound();
            }

            await _goalService.RemoveGoalAndWorkout(id);
            return NoContent();
        }

        /// <summary>
        /// Update Goal with new values
        /// </summary>
        /// <param name="id"></param>
        /// <param name="goalEditDTO"></param>
        /// <returns>Response Code</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> ChangeGoal(int id,GoalEditDTO goalEditDTO)
        {
            if (!_goalService.GoalExists(id))
            {
                return NotFound();
            }
            if (id != goalEditDTO.Id)
            {
                return BadRequest();
            }

            Goal newgoal = _mapper.Map<Goal>(goalEditDTO);
            await _goalService.UpdateGoal(newgoal);
            return Ok();
        }

        /// <summary>
        /// Get all goal by AccountID
        /// </summary>
        /// <param name="AccountId"></param>
        /// <returns></returns>
        [HttpGet("user/{AccountId}")]
        public async Task<ActionResult<IEnumerable<GoalReadDTO>>> GetGoalByAccountId(int AccountId)
        {
            return _mapper.Map<List<GoalReadDTO>>(await _goalService.GetAllGoalsById(AccountId));
        }

        /// <summary>
        /// Updates Workouts in Goal
        /// </summary>
        /// <param name="id"></param>
        /// <param name="workoutIds"></param>
        /// <returns>Response Code</returns>
        [HttpPut("{id}/workout")]
        public async Task<IActionResult> UpdateGoalWorkouts(int id, List<int> workoutIds)
        {
            if (!_goalService.GoalExists(id))
            {
                return NotFound();
            }

            try
            {
                await _goalService.UpdateGoalWorkouts(id, workoutIds);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalid charater.");
            }

            return NoContent();
        }

        [HttpPatch("{id}")]
        public async Task<IActionResult> PatchAccount(int id, GoalEditDTO dtoGoal)
        {
            if (!_goalService.GoalExists(id))
            {
                return NotFound();
            }

            var goal = await _goalService.GetGoaleById(id);
            Goal domainGoal = _mapper.Map<Goal>(dtoGoal);

            goal.StartDate = goal.StartDate;
            goal.Completed = domainGoal.Completed != false ? domainGoal.Completed : goal.Completed;
            goal.Difficulty = domainGoal.Difficulty != 0 ? domainGoal.Difficulty : goal.Difficulty;
            
            await _goalService.UpdateGoal(goal);

            return NoContent();
        }
    }
}

