﻿using AutoMapper;
using MeFit_API.Services.WorkoutService;
using MeFit_API.Models.DTO.ProfileDTO;
using MeFit_API.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net.Mime;
using System.Threading.Tasks;
using MeFit_API.Models.DTO.WorkoutDTO;
using MeFit_API.Models.DTO.SetDTO;
using MeFit_API.Helpers;
using Microsoft.AspNetCore.Http;

namespace MeFit_API.Controllers
{
    [Route("api/v1/workout")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class WorkoutController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IWorkoutService _workoutService;
        public WorkoutController(IMapper mapper, IWorkoutService workoutService)
        {
            _mapper = mapper;
            _workoutService = workoutService;
        }

        /// <summary>
        /// Fetches all the Workouts
        /// </summary>
        /// <returns>IEnmerable (List) of Workout</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<WorkoutReadDTO>>> GetWorkout()
        {
            return _mapper.Map<List<WorkoutReadDTO>>(await _workoutService.GetAllWorkouts());
        }

        [HttpGet("contributod")]
        public async Task<ActionResult<IEnumerable<WorkoutReadDTO>>> GetWorkoutMadeByContributor()
        {
            return _mapper.Map<List<WorkoutReadDTO>>(await _workoutService.GetAllContributedWorkouts());
        }

        /// <summary>
        /// Gets a specific workout by their Id
        /// </summary>
        /// <param name="id">The Id of Specified UserId / workout</param>
        /// <returns>A Instance of workoutReadDTO (workout)</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<WorkoutReadDTO>> GetWorkout(int id)
        {
            Workout workout = await _workoutService.GetWorkoutById(id);

            if (workout == null)
            {
                return NotFound();
            }

            return _mapper.Map<WorkoutReadDTO>(workout);
        }

        /// <summary>
        /// Gets a specific set by their Id
        /// </summary>
        /// <param name="id">The Id of Specified set</param>
        /// <returns>A Instance of setReadDTO (set)</returns>
        [HttpPost("getMultiple")]
        public async Task<ActionResult<IEnumerable<WorkoutReadDTO>>> GetListOfSet(List<int> ids)
        {
            List<WorkoutReadDTO> workouts = new();

            foreach (int id in ids)
            {
                Workout workout = await _workoutService.GetWorkoutById(id);

                if (workout == null)
                {
                    return NotFound();
                }

                workouts.Add(_mapper.Map<WorkoutReadDTO>(workout));
            }
            return workouts;
        }

        /// <summary>
        /// Adds a Acccount to Db
        /// </summary>
        /// <param name="dtoWorkout"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Workout>> PostWorkout(WorkoutCreateDTO dtoWorkout)
        {
            int tokenID = ValidateJWT.Validate(Request.Headers);

            if (tokenID == -1)
            {
                return BadRequest("You need to be logged in and have Contributor status");
            }

            int authLevel = await _workoutService.AccountAuthorized(tokenID);
            
            Workout domainWorkout = _mapper.Map<Workout>(dtoWorkout);
            
            if (authLevel < 1 && domainWorkout.MadeByContributor == true)
            {
                return Unauthorized("You need to be a Contributor or Admin to make Workouts as Contributor");
            }

            domainWorkout = await _workoutService.AddWorkout(domainWorkout);

            return CreatedAtAction("GetWorkout",
                new { id = domainWorkout.Id },
                _mapper.Map<WorkoutReadDTO>(domainWorkout));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutWorkout(int id, WorkoutEditDTO dtoWorkout)
        {
            if (id != dtoWorkout.Id)
            {
                return BadRequest();
            }

            if (!_workoutService.WorkoutExists(id))
            {
                return NotFound();
            }

            Workout domainWorkout = _mapper.Map<Workout>(dtoWorkout);
            await _workoutService.UpdateWorkout(domainWorkout);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteWorkout(int id)
        {
            int tokenID = ValidateJWT.Validate(Request.Headers);

            if (tokenID == -1)
            {
                return BadRequest("You need to be logged in and have Contributor status");
            }

            int authLevel = await _workoutService.AccountAuthorized(tokenID);

            if (authLevel < 1)
            {
                return Unauthorized("You need to be a Contributor or Admin");
            }

            if (!_workoutService.WorkoutExists(id))
            {
                return NotFound();
            }

            await _workoutService.RemoveWorkoutCascading(id);

            return NoContent();
        }


        [HttpPut("{id}/set")]
        public async Task<IActionResult> UpdateGoalWorkouts(int id, List<int> setIds)
        {
            if (!_workoutService.WorkoutExists(id))
            {
                return NotFound();
            }

            try
            {
                await _workoutService.UpdateWorkoutSets(id, setIds);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalid charater.");
            }

            return NoContent();
        }
        
        [ProducesResponseType(typeof(int[]), 200)]
        [HttpPost("Multiple")]
        public async Task<ActionResult<int[]>> PostWorkout(List<WorkoutCreateDTO2> dtoWorkouts)
        {
            int[] added = new int[7];

            for (int i = 0; i < dtoWorkouts.Count; i++)
            {
                
                if (dtoWorkouts[i].Sets == null)
                {
                    continue;
                }

                WorkoutCreateDTO wrdto = new WorkoutCreateDTO
                {
                    Title = dtoWorkouts[i].Title,
                    Type = dtoWorkouts[i].Type,
                    Completed = dtoWorkouts[i].Completed,
                    Difficulty = dtoWorkouts[i].Difficulty,
                    MadeByContributor = dtoWorkouts[i].MadeByContributor
                };

                Workout domainWorkout = _mapper.Map<Workout>(wrdto);
                domainWorkout = await _workoutService.AddWorkout(domainWorkout);

                await _workoutService.UpdateWorkoutSets(domainWorkout.Id, dtoWorkouts[i].Sets);

                added[i] = domainWorkout.Id;
            }
            return added;
        }

        /// <summary>
        /// Updates Workout with Specified Fields Only
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dtoWorkout"></param>
        /// <returns>Response Code</returns>
        [HttpPatch("{id}")]
        public async Task<IActionResult> PatchWorkout(int id, WorkoutEditDTO dtoWorkout)
        {
            int tokenID = ValidateJWT.Validate(Request.Headers);

            if (tokenID == -1)
            {
                return BadRequest("You need to be logged in and have Contributor status");
            }

            int authLevel = await _workoutService.AccountAuthorized(tokenID);

            if (authLevel < 1)
            {
                // return Unauthorized("You need to be a Contributor or Admin");
                return Problem(
                    type: "/docs/errors/forbidden",
                    title: "Authenticated user is not authorized.",
                    detail: $"You must have the Contributor or Admin role.",
                    statusCode: StatusCodes.Status403Forbidden,
                    instance: HttpContext.Request.Path
                );
            }

            if (!_workoutService.WorkoutExists(id))
            {
                return NotFound();
            }

            Workout toBeUpdated = await _workoutService.GetWorkoutById(id);
            Workout domainExercise = _mapper.Map<Workout>(dtoWorkout);

            toBeUpdated.Id = toBeUpdated.Id;
            toBeUpdated.Title = domainExercise.Title != null ? domainExercise.Title : toBeUpdated.Title;
            toBeUpdated.Type = domainExercise.Type != null ? domainExercise.Type : toBeUpdated.Type;
            toBeUpdated.Difficulty = domainExercise.Difficulty != 0 ? domainExercise.Difficulty : toBeUpdated.Difficulty;
            toBeUpdated.Completed = domainExercise.Completed != 0 ? domainExercise.Completed : toBeUpdated.Completed;
            toBeUpdated.MadeByContributor = domainExercise.MadeByContributor != false ? domainExercise.MadeByContributor : toBeUpdated.MadeByContributor;

            await _workoutService.UpdateWorkout(toBeUpdated);

            return NoContent();
        }
    }
}
