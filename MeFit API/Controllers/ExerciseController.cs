﻿using AutoMapper;
using MeFit_API.Services.ExerciseService;
using MeFit_API.Models;
using MeFit_API.Models.DTO.ExerciseDTO;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net.Mime;
using System.Threading.Tasks;
using MeFit_API.Helpers;

namespace MeFit_API.Controllers
{
    [Route("api/v1/exercise")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class ExerciseController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IExerciseService _exerciseService;

        public ExerciseController(IMapper mapper, IExerciseService exerciseService)
        {
            _mapper = mapper;
            _exerciseService = exerciseService;
        }

        /// <summary>
        /// Fetches all the Exercises
        /// </summary>
        /// <returns>IEnmerable (List) of Exercise</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ExerciseReadDTO>>> GetExercises()
        {
            return _mapper.Map<List<ExerciseReadDTO>>(_exerciseService.GetAllExercises());
        }

        /// <summary>
        /// Gets a specific Exercise by their Id
        /// </summary>
        /// <param name="id">The Id of Specified Exercise</param>
        /// <returns>A Instance of ExerciseReadDTO (Exercise)</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<ExerciseReadDTO>> GetExercise(int id)
        {
            Exercise exercise = await _exerciseService.GetExerciseById(id);

            if (exercise == null)
            {
                return NotFound();
            }

            return _mapper.Map<ExerciseReadDTO>(exercise);
        }

        /// <summary>
        /// Adds a new Exercise to DB
        /// </summary>
        /// <param name="dtoExercise"></param>
        /// <returns>A Instance of Created Exercise</returns>
        [HttpPost]
        public async Task<ActionResult<Exercise>> PostExercise(ExerciseCreateDTO dtoExercise)
        {
            int tokenID = ValidateJWT.Validate(Request.Headers);

            if (tokenID == -1)
            {
                return BadRequest("You need to be logged in and have Contributor status");
            }

            int authLevel = await _exerciseService.AccountAuthorized(tokenID);

            if (authLevel < 1)
            {
                return Unauthorized("You need to be a Contributor or Admin");
            }


            Exercise domainExercise = _mapper.Map<Exercise>(dtoExercise);

            domainExercise = await _exerciseService.AddExercise(domainExercise);

            return CreatedAtAction("GetExercise",
                new { id = domainExercise.Id },
                _mapper.Map<ExerciseReadDTO>(domainExercise));
        }
        
        /// <summary>
        /// Updates Exercise with new values.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dtoExercise"></param>
        /// <returns>Response Code</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutExercise(int id, ExerciseEditDTO dtoExercise)
        {
            if (id != dtoExercise.Id)
            {
                return BadRequest();
            }

            if (!_exerciseService.ExerciseExists(id))
            {
                return NotFound();
            }

            Exercise domainExercise = _mapper.Map<Exercise>(dtoExercise);
            await _exerciseService.UpdateExercise(domainExercise);

            return NoContent();
        }

        /// <summary>
        /// Deletes Seleted Exercise
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Response Code</returns>
        [HttpDelete("{id}")] //rolle
        public async Task<IActionResult> DeleteExercise(int id)
        {
            int tokenID = ValidateJWT.Validate(Request.Headers);

            if (tokenID == -1)
            {
                return BadRequest("You need to be logged in and have Contributor status");
            }

            int authLevel = await _exerciseService.AccountAuthorized(tokenID);

            if (authLevel < 1)
            {
                return Unauthorized("You need to be a Contributor or Admin");
            }

            if (!_exerciseService.ExerciseExists(id))
            {
                return NotFound();
            }

            await _exerciseService.RemoveExercise(id);

            return NoContent();
        }

        /// <summary>
        /// Updates Exercise with Specified Fields Only
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dtoExercise"></param>
        /// <returns>Response Code</returns>
        [HttpPatch("{id}")]
        public async Task<IActionResult> PatchExercise(int id, ExerciseEditDTO dtoExercise)
        {
            int tokenID = ValidateJWT.Validate(Request.Headers);

            if (tokenID == -1)
            {
                return BadRequest("You need to be logged in and have Contributor status");
            }

            int authLevel = await _exerciseService.AccountAuthorized(tokenID);

            if (authLevel < 1)
            {
                return Unauthorized("You need to be a Contributor or Admin");
            }

            if (!_exerciseService.ExerciseExists(id))
            {
                return NotFound();
            }

            Exercise toBeUpdated = await _exerciseService.GetExerciseById(id);
            Exercise domainExercise = _mapper.Map<Exercise>(dtoExercise);
            
            toBeUpdated.Id = toBeUpdated.Id;
            toBeUpdated.Name = domainExercise.Name != null ? domainExercise.Name : toBeUpdated.Name;
            toBeUpdated.VideoUrl = domainExercise.VideoUrl != null ? domainExercise.VideoUrl : toBeUpdated.VideoUrl;
            toBeUpdated.Description = domainExercise.Description != null ? domainExercise.Description : toBeUpdated.Description;
            toBeUpdated.Muscules = domainExercise.Muscules != null ? domainExercise.Muscules : toBeUpdated.Muscules;

            await _exerciseService.UpdateExercise(toBeUpdated);

            return NoContent();
        }
    }
}
