﻿using AutoMapper;
using MeFit_API.Services.ProgramService;
using MeFit_API.Models;
using MeFit_API.Models.Domain;
using MeFit_API.Models.DTO.TrainingProgramDTO;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net.Mime;
using System.Threading.Tasks;

namespace MeFit_API.Controllers
{
    [Route("api/v1/program")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class ProgramController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IProgramService _programService;

        public ProgramController(IMapper mapper, IProgramService programService)
        {
            _mapper = mapper;
            _programService = programService;
        }

        /// <summary>
        /// Fetches all the Accounts
        /// </summary>
        /// <returns>IEnmerable (List) of Account</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TrainingProgramReadDTO>>> GetPrograms()
        {
            return _mapper.Map<List<TrainingProgramReadDTO>>(await _programService.GetAllPrograms());
        }

        /// <summary>
        /// Gets a specific Account by their Id
        /// </summary>
        /// <param name="id">The Id of Specified UserId / Account</param>
        /// <returns>A Instance of AccountReadDTO (Account)</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<TrainingProgramReadDTO>> GetProgram(int id)
        {
            TrainingProgram program = await _programService.GetProgramById(id);

            if (program == null)
            {
                return NotFound();
            }

            return _mapper.Map<TrainingProgramReadDTO>(program);
        }

        /// <summary>
        /// Adds a Acccount to Db
        /// </summary>
        /// <param name="dtoAccount"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Account>> PostProgram(TrainingProgramCreateDTO dtoProgram)
        {
            TrainingProgram domainProgram = _mapper.Map<TrainingProgram>(dtoProgram);

            domainProgram = await _programService.AddProgram(domainProgram);

            return CreatedAtAction("GetProgram",
                new { id = domainProgram.Id },
                _mapper.Map<TrainingProgramReadDTO>(domainProgram));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutProgram(int id, TrainingProgramEditDTO dtoProgram)
        {
            if (id != dtoProgram.Id)
            {
                return BadRequest();
            }

            if (!_programService.ProgramExists(id))
            {
                return NotFound();
            }

            TrainingProgram domainProgram = _mapper.Map<TrainingProgram>(dtoProgram);
            await _programService.UpdateProgram(domainProgram);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProgram(int id)
        {
            if (!_programService.ProgramExists(id))
            {
                return NotFound();
            }

            await _programService.RemoveProgram(id);

            return NoContent();
        }

        [HttpPut("{id}/workout")]
        public async Task<IActionResult> UpdateMovieCharaters(int id, List<int> workoutIds)
        {
            if (!_programService.ProgramExists(id))
            {
                return NotFound();
            }

            try
            {
                await _programService.UpdateProgramWorkouts(id, workoutIds);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalid charater.");
            }

            return NoContent();
        }


        [ProducesResponseType(typeof(List<int>), 200)]
        [HttpPost("Multiple")]
        public async Task<ActionResult<int[]>> PostWorkout(TrainingProgramCreateDTO2 dtoProgram)
        {
            TrainingProgramCreateDTO tpdto = new TrainingProgramCreateDTO
            {
                Title = dtoProgram.Title,
                Category = dtoProgram.Category,
            };

            TrainingProgram domainProgram = _mapper.Map<TrainingProgram>(tpdto);
            domainProgram = await _programService.AddProgram(domainProgram);

            if (dtoProgram.Workouts != null)
            {
                await _programService.UpdateProgramWorkouts(domainProgram.Id, dtoProgram.Workouts);
            }

            return CreatedAtAction("GetProgram",
                new { id = domainProgram.Id },
                _mapper.Map<TrainingProgramReadDTO>(domainProgram));
        }

        [HttpPatch("{id}")]
        public async Task<IActionResult> PatchAccount(int id, TrainingProgramEditDTO2 dtoProgram)
        {
            if (!_programService.ProgramExists(id))
            {
                return NotFound();
            }

            var program = await _programService.GetProgramById(id);

            program.Title = dtoProgram.Title != "" ? dtoProgram.Title : program.Title;
            program.Category = dtoProgram.Category != "" ? dtoProgram.Category : program.Category;

            await _programService.UpdateProgram(program);
            if (dtoProgram.Workouts != null)
            {
                await _programService.UpdateProgramWorkouts(id, dtoProgram.Workouts);
            }

            return CreatedAtAction("GetProgram",
                new { id = id },
                _mapper.Map<TrainingProgramReadDTO>(program));
        }
    }
}
