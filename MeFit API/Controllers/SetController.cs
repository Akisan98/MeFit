﻿using AutoMapper;
using MeFit_API.Models;
using MeFit_API.Models.DTO.SetDTO;
using MeFit_API.Services.SetService;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net.Mime;
using System.Threading.Tasks;

namespace MeFit_API.Controllers
{
    [Route("api/v1/set")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class SetController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ISetService _setService;

        public SetController(IMapper mapper, ISetService setService)
        {
            _mapper = mapper;
            _setService = setService;
        }

        /// <summary>
        /// Fetches all the Sets
        /// </summary>
        /// <returns>IEnmerable (List) of Set</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SetReadDTO>>> GetSets()
        {
            return _mapper.Map<List<SetReadDTO>>(await _setService.GetAllSets());
        }

        /// <summary>
        /// Gets a specific set by their Id
        /// </summary>
        /// <param name="id">The Id of Specified set</param>
        /// <returns>A Instance of setReadDTO (set)</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<SetReadDTO>> GetSet(int id)
        {
            Set set = await _setService.GetSetById(id);

            if (set == null)
            {
                return NotFound();
            }

            return _mapper.Map<SetReadDTO>(set);
        }

        /// <summary>
        /// Gets a specific set by their Id
        /// </summary>
        /// <param name="id">The Id of Specified set</param>
        /// <returns>A Instance of setReadDTO (set)</returns>
        [HttpPost("getMultiple")]
        public async Task<ActionResult<IEnumerable<SetReadDTO>>> GetListOfSet(List<int> ids)
        {
            List<SetReadDTO> sets = new();

            foreach(int id in ids)
            {
                Set set = await _setService.GetSetById(id);

                if (set == null)
                {
                    return NotFound();
                }

                sets.Add(_mapper.Map<SetReadDTO>(set));
            }
            return sets;
        }

        /// <summary>
        /// Adds a Acccount to Db
        /// </summary>
        /// <param name="dtoWorkout"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Set>> PostWorkout(SetCreateDTO dtoSet)
        {
            Set domainSet = _mapper.Map<Set>(dtoSet);

            domainSet = await _setService.AddSet(domainSet);

            return CreatedAtAction("GetSet",
                new { id = domainSet.Id },
                _mapper.Map<SetReadDTO>(domainSet));
        }

        /// <summary>
        /// Adds a List of Sets to Db
        /// </summary>
        /// <param name="dtoSet"></param>
        /// <returns></returns>
        [ProducesResponseType(typeof(List<int>), 200)]
        [HttpPost("addMultiple")]
        public async Task<ActionResult<List<int>>> PostSetList(List<SetCreateDTO> dtoSetList)
        {
            List<int> added = new();

            foreach (SetCreateDTO dtoSet in dtoSetList)
            {
                Set domainSet = _mapper.Map<Set>(dtoSet);
                domainSet = await _setService.AddSet(domainSet);
                added.Add(domainSet.Id);
                // added2.Add(_mapper.Map<SetReadDTO>(domainSet));
            }

            return added;
            //return CreatedAtAction("GetListOfSet", formatted);
            //return NoContent();
        }

        /// <summary>
        /// Adds a List of Sets to Db
        /// </summary>
        /// <param name="dtoSet"></param>
        /// <returns></returns>
        [ProducesResponseType(typeof(List<List<int>>), 200)]
        [HttpPost("addMultiple2")]
        public async Task<ActionResult<List<List<int>>>> PostSetList(List<List<SetCreateDTO>> dtoSetListList)
        {
            List<List<int>> added = new();

            foreach (List<SetCreateDTO> dtoSetList in dtoSetListList)
            {
                Console.WriteLine(dtoSetList.ToString());
                List<int> currs = new();
                foreach (SetCreateDTO dtoSet in dtoSetList)
                {
                    Console.WriteLine(dtoSet.ToString());
                    Set domainSet = _mapper.Map<Set>(dtoSet);
                    domainSet = await _setService.AddSet(domainSet);
                    currs.Add(domainSet.Id);
                }

                added.Add(currs);
            }

            return added;
            //return CreatedAtAction("GetListOfSet", formatted);
            //return NoContent();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutWorkout(int id, SetEditDTO dtoSet)
        {
            if (id != dtoSet.Id)
            {
                return BadRequest();
            }

            if (!_setService.SetExists(id))
            {
                return NotFound();
            }

            Set domainSet = _mapper.Map<Set>(dtoSet);
            await _setService.UpdateSet(domainSet);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteWorkout(int id)
        {
            if (!_setService.SetExists(id))
            {
                return NotFound();
            }

            await _setService.RemoveSet(id);

            return NoContent();
        }
    }
}
