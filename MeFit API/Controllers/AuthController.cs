﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using backend_test.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Google.Apis.Auth;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using MeFit_API.Models;
using backend_test.Helpers;
using MeFit_API.Models.Domain;

namespace backend_test.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    public class AuthController : Controller
    {

        private IAuthService _authService;

        public AuthController(IAuthService authService)
        {
            this._authService = authService;
        }

        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<IActionResult> Google([FromBody] UserView userView)
        {
            var myIssuer = "http://meFitBackend.com";
            var myAudience = "http://meFitClient.com";

            try
            {
                var payload = GoogleJsonWebSignature.ValidateAsync(userView.tokenId, new GoogleJsonWebSignature.ValidationSettings()).Result;
                var account = await _authService.Authenticate(payload, userView.googleId);

                var claims = new[]
                {

                    new Claim(ClaimTypes.Name, account.Id.ToString()),
                    new Claim(JwtRegisteredClaimNames.Sub, Security.Encrypt(AppSettings.appSettings.JwtEmailEncryption,account.Email)),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),

                };

                var key = new SymmetricSecurityKey(Encoding.ASCII.GetBytes("Xasdfasdfasdfasdfasdfasdfasdfvs"));
                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                var token = new JwtSecurityToken(
                  issuer: myIssuer,
                  audience: myAudience,
                  claims,
                  expires: DateTime.Now.AddSeconds(60 * 60 * 24 * 30),
                  signingCredentials: creds);
                return Ok(new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token),
                    account
                });
            }
            catch (Exception ex)
            {
                //Unauthorized()
                return BadRequest(ex.Message);
            }

        }
    }
}

