﻿using MeFit_API.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using MeFit_API.Models.DTO.ProfileDTO;
using MeFit_API.Models;
using AutoMapper;
using Account = MeFit_API.Models.Account;
using MeFit_API.Models.DTO.GoalDTO;
using MeFit_API.Helpers;
using Microsoft.AspNetCore.JsonPatch;
using Newtonsoft.Json.Linq;
using Microsoft.EntityFrameworkCore;
using System.Security.Principal;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

// COPY OF ACCOUNT AS WE HAVE MERGED PROFILE AND USER INTO ACCOUNT
namespace MeFit_API.Controllers
{
    [Route("api/v1/user")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class UserController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IAccountService _accountService;
        public UserController(IMapper mapper, IAccountService accountService)
        {
            _mapper = mapper;
            _accountService = accountService;
        }

        /// <summary>
        /// Gets Currently Loged In Account
        /// </summary>
        /// <returns>A Instance of AccountReadDTO (Account)</returns>
        [HttpGet]
        public async Task<ActionResult<AccountReadDTO>> GetAccount()
        {
            // Doesn't Seem to Support 303 With Redirect, Only Empty Respose with Code 303
            int id = ValidateJWT.Validate(Request.Headers);
            Account profile = await _accountService.GetAccountById(id);

            if (!_accountService.AccountExists(id) || id == -1)
            {
                if (id == -1)
                {
                    return Unauthorized();
                }

                return NotFound();
            }

            return _mapper.Map<AccountReadDTO>(profile);
        }

        /// <summary>
        /// Gets a specific Account by their Id
        /// </summary>
        /// <param name="id">The Id of Specified Account</param>
        /// <returns>A Instance of AccountReadDTO (Account)</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<AccountReadDTO>> GetAccount(int id)
        {
            Account profile = await _accountService.GetAccountById(id);

            if (!_accountService.AccountExists(id))
            {
                return NotFound();
            }

            return _mapper.Map<AccountReadDTO>(profile);
        }

        /// <summary>
        /// Fetches all the Accounts
        /// </summary>
        /// <returns>IEnmerable (List) of Account</returns>
        [HttpGet("All")]
        public async Task<ActionResult<IEnumerable<AccountReadDTO>>> GetAccounts()
        {
            return _mapper.Map<List<AccountReadDTO>>(await _accountService.GetAllAccounts());
        }

        /// <summary>
        /// Adds a new Acccount to DB
        /// </summary>
        /// <param name="dtoAccount"></param>
        /// <returns>Instance of Created Account</returns>
        [HttpPost]
        public async Task<ActionResult<Account>> PostAccount(AccountCreateDTO dtoAccount)
        {
            Account domainAccount = _mapper.Map<Account>(dtoAccount);
            domainAccount = await _accountService.AddAccount(domainAccount);

            return CreatedAtAction("GetAccount",
                new { id = domainAccount.Id },
                _mapper.Map<AccountReadDTO>(domainAccount));
        }



        /// <summary>
        /// Update exsisting Account with new data
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dtoAccount"></param>
        /// <returns>Response Code</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAccount(int id, AccountEditDTO dtoAccount)
        {


            if (!_accountService.AccountExists(id))
            {
                return NotFound();
            }

            Account domainAccount = _mapper.Map<Account>(dtoAccount);
            domainAccount.Id = id;
            await _accountService.UpdateAccount(domainAccount);

            return NoContent();
        }


        [HttpPut("update")]
        public async Task<IActionResult> PutUpdateAccount(AccountEditDTO dtoAccount)
        {
            int id = ValidateJWT.Validate(Request.Headers);


            if (!_accountService.AccountExists(id))
            {
                return NotFound();
            }

            Account domainAccount = _mapper.Map<Account>(dtoAccount);
            domainAccount.Id = id;
            await _accountService.UpdateAccount(domainAccount);

            return NoContent();
        }

        /// <summary>
        /// Deletes Spescifed Account
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Response Code</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAccount(int id)
        {
            int tokenID = ValidateJWT.Validate(Request.Headers);
            Account profile = await _accountService.GetAccountById(tokenID);

            if (profile.Admin != 3 && profile.Id != id)
            {
                BadRequest();
            }

            if (!_accountService.AccountExists(id))
            {
                return NotFound();
            }

            await _accountService.RemoveAccount(id);

            return NoContent();
        }

        /*/// <summary>
        /// Get all Account where User is Requesting Contributor Accsess.
        /// </summary>
        /// <returns>List of Accounts</returns>
        [HttpGet("/contributor/requesting")]
        public async Task<ActionResult<IEnumerable<AccountReadDTO>>> GetRequests()
        {
            return _mapper.Map<List<AccountReadDTO>>(await _accountService.GetAllRequests());
        }*/

        /// <summary>
        /// Updates Account with Goals
        /// </summary>
        /// <param name="id"></param>
        /// <param name="goalIds"></param>
        /// <returns>Response Code</returns>
        [HttpPut("{id}/goal")]
        public async Task<IActionResult> UpdateAccountGoals(int id, List<int> goalIds)
        {
            if (!_accountService.AccountExists(id))
            {
                return NotFound();
            }

            try
            {
                await _accountService.UpdateAccountGoals(id, goalIds);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalid charater.");
            }

            return NoContent();
        }

        // Propper PATCH - https://www.roundthecode.com/dotnet/asp-net-core-web-api/asp-net-core-api-how-to-perform-partial-update-using-http-patch

        /// <summary>
        /// Updates Account with Specified Fields Only
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dtoAccount"></param>
        /// <returns>Response Code</returns>
        [HttpPatch("{id}")]
        public async Task<IActionResult> PatchAccount(int id, AccountEditDTO dtoAccount)
        {
            int tokenID = ValidateJWT.Validate(Request.Headers);
            Account profile = await _accountService.GetAccountById(tokenID);

            if (!_accountService.AccountExists(id))
            {
                return NotFound();
            }

            var account = await _accountService.GetAccountById(id);
            Account domainAccount = _mapper.Map<Account>(dtoAccount);

            if (profile.Admin != 3 && account.Admin != 3 && domainAccount.Admin == 3)
            {
                return Problem(
                    type: "/docs/errors/forbidden",
                    title: "Authenticated user is not authorized.",
                    detail: $"User '{profile.FirstName}' must have the Admin role.",
                    statusCode: StatusCodes.Status403Forbidden,
                    instance: HttpContext.Request.Path
                );
            }

            account.FitnessLevel = domainAccount.FitnessLevel != 0 ? domainAccount.FitnessLevel : account.FitnessLevel;
            account.ImageUrl = domainAccount.ImageUrl != null ? domainAccount.ImageUrl : account.ImageUrl;
            account.Height = domainAccount.Height != 0 ? domainAccount.Height : account.Height;
            account.Weight = domainAccount.Weight != 0 ? domainAccount.Weight : account.Weight;
            account.Admin = domainAccount.Admin != 1 ? domainAccount.Admin : account.Admin;
            account.Contributer = domainAccount.Contributer != 1 ? domainAccount.Contributer : account.Contributer;
            account.FirstName = domainAccount.FirstName != null ? domainAccount.FirstName : account.FirstName;
            account.LastName = domainAccount.LastName != null ? domainAccount.LastName : account.LastName;
            account.Email = domainAccount.Email != null ? domainAccount.Email : account.Email;
            account.GoogleId = domainAccount.GoogleId != null ? domainAccount.GoogleId : account.GoogleId;

            await _accountService.UpdateAccount(account);

            return NoContent();
        }
    }
}
