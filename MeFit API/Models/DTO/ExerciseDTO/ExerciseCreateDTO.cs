﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MeFit_API.Models.DTO.ExerciseDTO
{
    public class ExerciseCreateDTO
    {
        
        public string Name { get; set; }
        public string VideoUrl { get; set; }
        public string Description { get; set; }
        // public List<Workout> Workouts { get; set; }
        public string Muscules { get; set; }
    }
}
