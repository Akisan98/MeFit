﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeFit_API.Models.DTO.SetDTO
{
    public class SetEditDTO
    {
        public int Id { get; set; }
        public int Reps { get; set; }
        public int ExerciseId { get; set; }
        public int Sets { get; set; }
    }
}
