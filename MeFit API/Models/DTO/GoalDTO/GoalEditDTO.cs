﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeFit_API.Models.DTO.GoalDTO
{
    public class GoalEditDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime StartDate { get; set; }

        public bool Completed { get; set; }

        public int Difficulty { get; set; }
        //public int Profile { get; set; }
        public int AccountId { get; set; }
        // public List<int> Workouts { get; set; }
    }
}
