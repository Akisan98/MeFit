﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeFit_API.Models.DTO.ProfileDTO
{
    public class AccountCreateDTO
    {
        public int FitnessLevel { get; set; }
        public string ImageUrl { get; set; }
        public int Height { get; set; }
        public int Weight { get; set; }
        public int Admin { get; set; } 
        public int Contributer { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string GoogleId { get; set; }
    }
}
