﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeFit_API.Models.DTO.TrainingProgramDTO
{
    public class TrainingProgramCreateDTO
    {
        public string Title { get; set; }
        public string Category { get; set; }
    }

    public class TrainingProgramCreateDTO2
    {
        public string Title { get; set; }
        public string Category { get; set; }
        public List<int> Workouts { get; set; }
    }
}
