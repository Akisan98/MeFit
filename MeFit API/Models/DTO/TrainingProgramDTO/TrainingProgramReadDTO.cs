﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeFit_API.Models.DTO.TrainingProgramDTO
{
    public class TrainingProgramReadDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Category { get; set; }
        public List<int> Workouts { get; set; }
    }
}
