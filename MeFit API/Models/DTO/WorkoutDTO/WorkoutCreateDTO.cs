﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeFit_API.Models.DTO.WorkoutDTO
{
    public class WorkoutCreateDTO
    {
        public string Title { get; set; }
        public string Type { get; set; }
        public int Difficulty { get; set; }
        public bool MadeByContributor { get; set; }
        public int Completed { get; set; }
        //public List<Set> Sets { get; set; }
        //public List<Goal> Goals { get; set; }
    }

    public class WorkoutCreateDTO2
    {
        public string Title { get; set; }
        public string Type { get; set; }
        public int Difficulty { get; set; }
        public bool MadeByContributor { get; set; }
        public int Completed { get; set; }
        public List<int> Sets { get; set; }
        //public List<Goal> Goals { get; set; }
    }
}
