﻿using System.ComponentModel.DataAnnotations.Schema;

namespace MeFit_API.Models
{
    [Table("Muscule")]
    public class Muscule
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
