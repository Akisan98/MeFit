﻿using MeFit_API.Models.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MeFit_API.Models
{
    [Table("Workout")]
    public class Workout
    {
        public int Id {  get; set; }
        public string Title { get; set; }

        [Required]
        public string Type { get; set; }
        public int Difficulty { get; set; }
        public int Completed { get; set; }
        
        [Required]
        public bool MadeByContributor { get; set; }
        // public ICollection<Set> Sets { get; set; }
        public ICollection<Goal> Goals { get; set; }
        //public ICollection<Exercise> Exercises { get; set; }
        public ICollection<TrainingProgram> Programs { get; set; }
        public ICollection<Set> Sets { get; set; }


    }
}
