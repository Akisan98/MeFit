﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace MeFit_API.Models
{
    [Table("User")]
    public class User
    {
        public int Id { get; set; }
        
        [Required]
        [MaxLength(26)]
        public string FirstName {  get; set; }
        
        [Required]
        [MaxLength(26)]
        public string LastName {  get; set; }
        
        [Required]
        [MaxLength(15)]
        public string Role {  get; set; }
        
        [MaxLength(100)]
        public string Email {  get; set; }
       
        public string GoogleId { get; set; }
        
        public int AccountId { get; set; }
        public Account Account { get; set; }

        public string imageUrl { get; set; }
    }
    public class UserView
    {
        public string tokenId { get; set; }
        public string googleId { get; set; }
    }
    
}
