﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MeFit_API.Models
{
    [Table("Exercise")]
    public class Exercise
    {
        public int Id { get; set; }
        
        // [Required]
        public string Name { get; set; }
        public string VideoUrl { get; set; }
        public string Description { get; set; }
        //public ICollection<Workout> Workouts { get; set; }
        //public ICollection<Muscule> Muscules { get; set; }
        public string Muscules { get; set; }
    }
}
