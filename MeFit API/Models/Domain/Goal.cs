﻿using MeFit_API.Models.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MeFit_API.Models
{
    [Table("Goal")]
    public class Goal
    {
        // [ForeignKey("Account")]
        
        public int Id { get; set; }
        public string Title { get; set; }

        [Required]
        public DateTime StartDate { get; set; }

        [Required]
        public bool Completed { get; set; }

        public int Difficulty { get; set; }

        public int AccountId { get; set; }
        public Account Account { get; set; }

        public ICollection<Workout> Workouts { get; set; }
    }
}
