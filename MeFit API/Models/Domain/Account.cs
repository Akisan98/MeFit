﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MeFit_API.Models
{
    [Table("Account")]
    public class Account
    {
        //[ForeignKey("User")]
        public int Id { get; set; }
        public int FitnessLevel { get; set; }

        [MaxLength(255)]
        public string ImageUrl { get; set; }

        public int Height { get; set; }
        public int Weight { get; set; }
        public ICollection<Goal> Goals { get; set; }

        public int Admin { get; set; } // 1 - No, 2 - Pending, 3 - True
        public int Contributer { get; set; } 

        // public string GoogleId { get; set; }
        //[Key()] //-- To Make this PK instead of using Id
        [Required]
        [MaxLength(26)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(26)]
        public string LastName { get; set; }
        [MaxLength(100)]
        public string Email { get; set; }

        public string GoogleId { get; set; }

    }
}

