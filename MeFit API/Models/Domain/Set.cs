﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MeFit_API.Models
{
    [Table("Set")]
    public class Set
    {
        public int Id {  get; set; }
        public int Reps { get; set; }
        public int Sets { get; set; }

        // public int WorkoutId {  get; set; }
        public ICollection<Workout> Workouts { get; set; }

        //[ForeignKey("Exercise")]
        public int ExerciseId { get; set; }
        public Exercise Exercise { get; set; }
    }
}
