﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MeFit_API.Models.Domain
{
    [Table("Program")]
    public class TrainingProgram
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Category { get; set; }
        public ICollection<Workout> Workouts { get; set; }
    }
}
