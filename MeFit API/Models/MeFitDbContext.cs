﻿using MeFit_API.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MeFit_API.Models
{
    public class MeFitDbContext : DbContext
    {
        // TODO: Change to Singular Form
        //public DbSet<User> Users {  get; set; }
        public DbSet<Workout> Workouts { get; set; }
        public DbSet<Goal> Goals { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Exercise> Exercises { get; set; }
        public DbSet<TrainingProgram> Programs { get; set; }
        public DbSet<Set> Sets { get; set; }

        public MeFitDbContext([NotNullAttribute] DbContextOptions options) : base(options) { }

        /*protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=ND-5CG9030M7M\\SQLEXPRESS;Initial Catalog=MeFitDB;Integrated Security=True;");
            // optionsBuilder.UseSqlServer("Data Source=ND-5CG92747K7\\SQLEXPRESS;Initial Catalog=MeFitDB;Integrated Security=True;");
        }*/


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // User / Profile / Account
            modelBuilder.Entity<Account>().HasData(
                new Account
                {
                    Id = 1,
                    FirstName = "Test",
                    LastName = "Account 1",
                    Email = "NOHAVE",
                    GoogleId = "1",
                    Admin = 1,
                    ImageUrl = "NO_IMAGE",
                    Height = 170,
                    Weight = 90,
                    Contributer = 2,
                    FitnessLevel = 5
                }
            );

            modelBuilder.Entity<Account>().HasData(
                new Account
                {
                    Id = 2,
                    FirstName = "Test",
                    LastName = "Account 2",
                    Email = "NOHAVE",
                    GoogleId = "2",
                    Admin = 3,
                    ImageUrl = "NO_IMAGE",
                    Height = 180,
                    Weight = 70,
                    Contributer = 3,
                    FitnessLevel = 100
                }
            );

            // Goal
            modelBuilder.Entity<Goal>().HasData(
                new Goal
                {
                    Id = 1,
                    Title = "Summer Body",
                    StartDate = new DateTime(2021, 10, 20),
                    Completed = false,
                    Difficulty = 3,
                    AccountId = 1
                }
           );

            modelBuilder.Entity<Goal>().HasData(
                new Goal
                {
                    Id = 2,
                    Title = "Find Your Max BPM",
                    StartDate = new DateTime(2021, 10, 07),
                    Completed = true,
                    Difficulty = 1,
                    AccountId = 1,

                }
            );

            modelBuilder.Entity<Goal>().HasData(
                new Goal
                {
                    Id = 3,
                    Title = "Get Stronger",
                    StartDate = new DateTime(2021, 10, 11),
                    Completed = true,
                    Difficulty = 45,
                    AccountId = 2
                }
            );

            
            
            // Exercise
            modelBuilder.Entity<Exercise>().HasData(
                new Exercise
                {
                    Id = 1,
                    Name = "Push Ups",
                    VideoUrl = "https://www.youtube.com/watch?v=IODxDxX7oi4",
                    Description = "A push-up (sometimes called a press-up in British English) is a common calisthenics exercise beginning from the prone position. ",
                    Muscules = "Triceps, Pectorals (Chest), Shoulders (Deltoids)"
                }
            );

            modelBuilder.Entity<Exercise>().HasData(
                new Exercise
                {
                    Id = 2,
                    Name = "Running",
                    VideoUrl = "https://www.youtube.com/watch?v=_kGESn8ArrU",
                    Description = "The action or movement of a runner",
                    Muscules = "Glutes, Hamstrings, Quads"
                }
            );

            modelBuilder.Entity<Exercise>().HasData(
                new Exercise
                {
                    Id = 3,
                    Name = "Jogging",
                    VideoUrl = "https://www.youtube.com/watch?v=Hv5UvqcsIfY",
                    Description = "The activity of running at a steady, gentle pace as a form of physical exercise",
                    Muscules = "Glutes, Hamstrings, Quads"
                }
            );

            modelBuilder.Entity<Exercise>().HasData(
                new Exercise
                {
                    Id = 4,
                    Name = "DB Lateral Raises",
                    VideoUrl = "https://www.youtube.com/watch?v=Hv5UvqcsIfY",
                    Description = "A push-up (sometimes called a press-up in British English) is a common calisthenics exercise beginning from the prone position. ",

                    Muscules = "Quads, Triceps"
                }
            );
            modelBuilder.Entity<Exercise>().HasData(
                new Exercise
                {
                    Id = 5,
                    Name = "Overhead DB Extensions",
                    VideoUrl = "https://www.youtube.com/watch?v=Hv5UvqcsIfY",
                    Description = "The action or movement of a runner",

                    Muscules = "Biceps, Quads"
                }
            ); 
            modelBuilder.Entity<Exercise>().HasData(
                 new Exercise
                 {
                     Id = 6,
                     Name = "Barbbell Squat",
                     VideoUrl = "https://www.youtube.com/watch?v=Hv5UvqcsIfY",
                     Description = "The activity of running at a steady, gentle pace as a form of physical exercise",
                     Muscules = "Biceps, Shoulders"
                 }
             );
            modelBuilder.Entity<Exercise>().HasData(
                new Exercise
                {
                    Id = 7,
                    Name = "Conventional Deadlift",
                    VideoUrl = "https://www.youtube.com/watch?v=Hv5UvqcsIfY",
                    Description = "A push-up (sometimes called a press-up in British English) is a common calisthenics exercise beginning from the prone position. ",

                    Muscules = "Shoulders, Triceps"
                }
            );

            modelBuilder.Entity<Exercise>().HasData(
                new Exercise
                {
                    Id = 8,
                    Name = "Bulgarian Split Squat",
                    VideoUrl = "https://www.youtube.com/watch?v=Hv5UvqcsIfY",
                    Description = "The action or movement of a runner",

                    Muscules = "Glutes, Triceps"
                }
            );
            modelBuilder.Entity<Exercise>().HasData(
                new Exercise
                {
                    Id = 9,
                    Name = "Single Leg Hip Thrust",
                    VideoUrl = "https://www.youtube.com/watch?v=Hv5UvqcsIfY",
                    Description = "The activity of running at a steady, gentle pace as a form of physical exercise",
                    Muscules = "Legs, Hip"
                }
            );
            modelBuilder.Entity<Exercise>().HasData(
                new Exercise
                {
                    Id = 10,
                    Name = "DB Front Squat",
                    VideoUrl = "https://www.youtube.com/watch?v=Hv5UvqcsIfY",
                    Description = "A push-up (sometimes called a press-up in British English) is a common calisthenics exercise beginning from the prone position. ",

                    Muscules = "Legs, Shoulders"
                }
            );
            modelBuilder.Entity<Exercise>().HasData(
                new Exercise
                {
                    Id = 11,
                    Name = "Weighted Pull Ups",
                    VideoUrl = "https://www.youtube.com/watch?v=Hv5UvqcsIfY",
                    Description = "The action or movement of a runner",

                    Muscules = "Legs, Triceps"
                }
            );
            modelBuilder.Entity<Exercise>().HasData(
                new Exercise
                {
                    Id = 12,
                    Name = "DB Bench Press",
                    VideoUrl = "https://www.youtube.com/watch?v=Hv5UvqcsIfY",
                    Description = "A push-up (sometimes called a press-up in British English) is a common calisthenics exercise beginning from the prone position. ",

                    Muscules = "Biceps, Triceps, Bench Muscle"
                }
            );
            modelBuilder.Entity<Exercise>().HasData(
                new Exercise
                {
                    Id = 13,
                    Name = "Seated Row",
                    VideoUrl = "https://www.youtube.com/watch?v=Hv5UvqcsIfY",
                    Description = "The activity of running at a steady, gentle pace as a form of physical exercise",
                    Muscules = "Biceps, Triceps, Gluts"
                }
            );
            modelBuilder.Entity<Exercise>().HasData(
                new Exercise
                {
                    Id = 14,
                    Name = "DB Reverse Flies",
                    VideoUrl = "https://www.youtube.com/watch?v=Hv5UvqcsIfY",
                    Description = "The action or movement of a runner",

                    Muscules = "Hip, Legs"
                }
            );
            modelBuilder.Entity<Exercise>().HasData(
                new Exercise
                {
                    Id = 15,
                    Name = "Band Pulls",
                    VideoUrl = "https://www.youtube.com/watch?v=Hv5UvqcsIfY",
                    Description = "A push-up (sometimes called a press-up in British English) is a common calisthenics exercise beginning from the prone position. ",

                    Muscules = "Biceps, Triceps, Quad"
                }
            );

            // Program
            modelBuilder.Entity<TrainingProgram>().HasData(
                new TrainingProgram
                {
                    Id = 1,
                    Title = "Workout program for beginners",
                    Category = "Flexibility",
                }
            );

            modelBuilder.Entity<TrainingProgram>().HasData(
                new TrainingProgram
                {
                    Id = 2,
                    Title = "Get Thicc Legs",
                    Category = "Strength",
                }
            );


            // Map one to Many relationship  |  Account --> Goal
            modelBuilder.Entity<Goal>()
                .HasOne(g => g.Account)
                .WithMany(a => a.Goals)
                .IsRequired();

            // Many-to-Many  |  Goal --> Workout
            modelBuilder.Entity<Goal>()
                .HasMany(t => t.Workouts)
                .WithMany(t => t.Goals)
                .UsingEntity<Dictionary<string, object>>(
                    "GoalWorkouts",
                    r => r.HasOne<Workout>().WithMany().HasForeignKey("WorkoutId"),
                    l => l.HasOne<Goal>().WithMany().HasForeignKey("GoalId"),
                    je =>
                    {
                        je.HasKey("WorkoutId", "GoalId");
                        je.HasData(
                            new { WorkoutId = 1, GoalId = 1 },
                            new { WorkoutId = 2, GoalId = 1 },
                            new { WorkoutId = 3, GoalId = 2 },
                            new { WorkoutId = 3, GoalId = 3 }
                        );
                    }
                );

            modelBuilder.Entity<TrainingProgram>()
                .HasMany(u => u.Workouts)
                .WithMany(b => b.Programs) // <- no parameter here because there is no navigation property
                .UsingEntity<Dictionary<string, object>>(
                    "ProgramWorkouts",
                    r => r.HasOne<Workout>().WithMany().HasForeignKey("WorkoutId"),
                    l => l.HasOne<TrainingProgram>().WithMany().HasForeignKey("ProgramId"),
                    je =>
                    {
                        je.HasKey("ProgramId", "WorkoutId");
                        je.HasData(
                            new { ProgramId = 1, WorkoutId = 1 },
                            new { ProgramId = 1, WorkoutId = 2 },
                            new { ProgramId = 2, WorkoutId = 3 }
                            //new { ProgramId = 3, WorkoutId = 4 }
                        );
                    }
                );

            // Workout
            modelBuilder.Entity<Workout>().HasData(
                new Workout
                {
                    Id = 1,
                    Title = "Leg day",
                    Type = "Legs",
                    Difficulty = 5,
                    Completed = 0,
                    MadeByContributor = true,
                }
            );

            modelBuilder.Entity<Workout>().HasData(
                new Workout
                {
                    Id = 2,
                    Title = "Arm day",
                    Type = "Arms",
                    Difficulty = 3,
                    Completed = 1,
                    MadeByContributor = true,
                }
            );

            modelBuilder.Entity<Workout>().HasData(
                new Workout
                {
                    Id = 3,
                    Title = "Graveyard Shift",
                    Type = "Strenght, Tone",
                    Difficulty = 2,
                    Completed = 0,
                    MadeByContributor = true,
                }
            );

            // Set
            modelBuilder.Entity<Set>().HasData(
                new Set
                {
                    Id = 1,
                    Reps = 5,
                    Sets = 20,
                    ExerciseId = 1,
                }
            );

            modelBuilder.Entity<Set>().HasData(
               new Set
               {
                   Id = 2,
                   Reps = 1,
                   Sets = 1,
                   ExerciseId = 2,
               }
           );

            modelBuilder.Entity<Set>()
                .HasMany(u => u.Workouts)
                .WithMany(b => b.Sets)
                .UsingEntity<Dictionary<string, object>>(
                    "SetWorkouts",
                    r => r.HasOne<Workout>().WithMany().HasForeignKey("WorkoutId"),
                    l => l.HasOne<Set>().WithMany().HasForeignKey("SetId"),
                    je =>
                    {
                        je.HasKey("SetId", "WorkoutId");
                        je.HasData(
                            new { SetId = 1, WorkoutId = 1 },
                            new { SetId = 2, WorkoutId = 1 },
                            new { SetId = 2, WorkoutId = 2 }
                            //new { ProgramId = 3, WorkoutId = 4 }
                        );
                    }
                );

        }
    }
}
