﻿using System;
using System.Linq;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;


namespace MeFit_API.Helpers
{
    public class ValidateJWT
    {
        public static int Validate(Microsoft.AspNetCore.Http.IHeaderDictionary header)
        {
            string token = header["token"];
            if (string.IsNullOrEmpty(token))
            {
                // Takes care of No Token without crash
                return -1;
            }
            token = token.Replace("\\", "").Replace("\"", "");
            var tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken validatedToken;
            var myIssuer = "http://meFitBackend.com";
            var myAudience = "http://meFitClient.com";
            try
            {
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidIssuer = myIssuer,
                    ValidAudience = myAudience,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes("Xasdfasdfasdfasdfasdfasdfasdfvs"))

                }, out validatedToken);
            }
            catch (Exception ex)
            {
                return -1;
            }

            var id = GetClaim(token, ClaimTypes.Name.ToString());
            return Int32.Parse(id);

        }
        public static string GetClaim(string token, string claimType)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var securityToken = tokenHandler.ReadToken(token) as JwtSecurityToken;

            var stringClaimValue = securityToken.Claims.First(claim => claim.Type == claimType).Value;
            return stringClaimValue;
        }
    }
}
