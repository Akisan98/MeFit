﻿using System.Threading.Tasks;
using System.Collections.Generic;
using System;
using System.Linq;
using MeFit_API.Models;
using Google.Apis.Auth;

namespace backend_test.Services
{
    public interface IAuthService
    {

        Task<Account> Authenticate(Google.Apis.Auth.GoogleJsonWebSignature.Payload payload, string googleID);
    }

    public class AuthService : IAuthService
    {
        private readonly MeFitDbContext _context;
        public AuthService(MeFitDbContext context)
        {
            _context = context;
        }

        public async Task<Account> Authenticate(GoogleJsonWebSignature.Payload payload, string googleID)
        {
            await Task.Delay(1);
            return await FindUserOrAdd(payload, googleID);
        }

        private async Task<Account> FindUserOrAdd(GoogleJsonWebSignature.Payload payload, string googleID)
        {


            var account = _context.Accounts.SingleOrDefault(c => c.GoogleId == googleID);
            
            if (account == null)
            {
                account = new Account()
                {

                    FirstName = payload.GivenName ?? "No Name",
                    LastName = payload.FamilyName ?? "",
                    Email = payload.Email ?? "",
                    FitnessLevel = 0,
                    GoogleId = googleID ?? "",
                    ImageUrl = payload.Picture ?? "",
                    Height = 0,
                    Weight = 0,
                    Admin = 1,
                    Contributer = 0
                };
                 _context.Accounts.Add(account);
                 await _context.SaveChangesAsync();

            }
            return account;

        }
    }
}