﻿using MeFit_API.Models;
using Microsoft.AspNetCore.JsonPatch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeFit_API.Services
{
    public interface IAccountService
    {
        public Task<IEnumerable<Account>> GetAllAccounts();
        public Task<Account> GetAccountById(int id);
        public Task<Account> AddAccount(Account account);
        public Task UpdateAccount(Account account);
        // public Task<IEnumerable<Profile>> UpdateProfileGoals(int id, List<Goal> goals);
        public Task RemoveAccount(int id);
        public bool AccountExists(int id);

        public Task<Account> GetAllRequests();
        public Task UpdateAccountGoals(int accountId, List<int> goalIds);

        public Task<Account> doUpdate(Account account, Account domainAccount, string[] n);
    }
}
