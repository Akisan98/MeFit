﻿using AutoMapper;
using MeFit_API.Models;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;

namespace MeFit_API.Services.ProfileService
{
    public class AccountService : IAccountService
    {
        private readonly MeFitDbContext _context;

        public AccountService(MeFitDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Account>> GetAllAccounts()
        {
            return await _context.Accounts
                .Include(a => a.Goals)
                .ToListAsync();
        }

        public async Task<Account> GetAccountById(int id)
        {
            // return await _context.Accounts.FindAsync(id); -- No Ref to Goals
            return await _context.Accounts.Where(a => a.Id == id).Include(a => a.Goals).FirstOrDefaultAsync();
        }

        public async Task<Account> GetAllRequests()
        {
            // return await _context.Accounts.FindAsync(id); -- No Ref to Goals
            return await _context.Accounts.Where(a => a.Contributer == 1).Include(a => a.Goals).FirstOrDefaultAsync();
        }

        public async Task<Account> AddAccount(Account account)
        {
            _context.Accounts.Add(account);
            await _context.SaveChangesAsync();
            return account;
        }

        public async Task UpdateAccount(Account account)
        {
            _context.Entry(account).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
        public async Task RemoveAccount(int id)
        {
            var account = await _context.Accounts.FindAsync(id);
            _context.Accounts.Remove(account);
            await _context.SaveChangesAsync();
        }

        public bool AccountExists(int id)
        {
            return _context.Accounts.Any(e => e.Id == id);
        }

        public async Task UpdateAccountGoals(int accountId, List<int> goalIds)
        {
            Account accountToUpdate = await _context.Accounts
                .Include(a => a.Goals)
                .Where(a => a.Id == accountId)
                .FirstAsync();

            // Loop through Goals
            List<Goal> goals = new();
            foreach (int goal in goalIds)
            {
                Goal goalToAdd = await _context.Goals.FindAsync(goal);
                if (goalToAdd == null)
                {
                    throw new KeyNotFoundException();
                }
                goals.Add(goalToAdd);
            }

            accountToUpdate.Goals = goals;
            await _context.SaveChangesAsync();
        }

        public async Task<Account> doUpdate(Account account, Account domainAccount, string[] n)
        {
            foreach (var property in n)
            {
                _context.Entry(account).Property(property).CurrentValue = _context.Entry(domainAccount).Property(property).CurrentValue;
                _context.Entry(account).Property(property).IsModified = true;
            }

            return account;

        }


    }
}
