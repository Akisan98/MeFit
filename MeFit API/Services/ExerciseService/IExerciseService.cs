﻿using MeFit_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeFit_API.Services.ExerciseService
{
    public interface IExerciseService
    {
        public IEnumerable<Exercise> GetAllExercises();
        public Task<Exercise> GetExerciseById(int id);
        public Task<Exercise> AddExercise(Exercise exercise);
        public Task UpdateExercise(Exercise exercise);
        // public Task<IEnumerable<Profile>> UpdateProfileGoals(int id, List<Goal> goals);
        public Task RemoveExercise(int id);
        // public Task<IEnumerable<Profile>> ProfileExsists(int id);
        public bool ExerciseExists(int id);
        public Task<int> AccountAuthorized(int accountID);
    }
}
