﻿using MeFit_API.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeFit_API.Services.ExerciseService
{
    public class ExerciseService : IExerciseService
    {
        private readonly MeFitDbContext _context;

        public ExerciseService(MeFitDbContext context)
        {
            _context = context;
        }

        public async Task<Exercise> AddExercise(Exercise exercise)
        {
            _context.Exercises.Add(exercise);
            await _context.SaveChangesAsync();
            return exercise;
        }

        // Exercise List Sorted by Muscle Group
        public IEnumerable<Exercise> GetAllExercises()
        {
            return _context.Exercises.ToList().OrderBy(x => x.Muscules);
        }

        public async Task<Exercise> GetExerciseById(int id)
        {
            return await _context.Exercises.FindAsync(id);
        }

        public async Task RemoveExercise(int id)
        {
            var exercise = await _context.Exercises.FindAsync(id);
            _context.Exercises.Remove(exercise);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateExercise(Exercise exercise)
        {
            _context.Entry(exercise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public bool ExerciseExists(int id)
        {
            return _context.Exercises.Any(e => e.Id == id);
        }

        public async Task<int> AccountAuthorized(int accountID)
        {
            // 0 - Normal User
            // 1 - Contributer 
            // 2 - Admin

            Account account = await _context.Accounts.FindAsync(accountID);
            
            if (account.Admin == 3) { return 2; }
            if (account.Contributer == 3) { return 1; }
            return 0;
        }
    }
}
