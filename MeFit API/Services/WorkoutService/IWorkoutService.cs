﻿using MeFit_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeFit_API.Services.WorkoutService
{
    public interface IWorkoutService
    {
        public Task<IEnumerable<Workout>> GetAllWorkouts();
        public Task<Workout> GetWorkoutById(int id);
        public Task<Workout> AddWorkout(Workout workout);
        public Task UpdateWorkout(Workout workout);
        public Task RemoveWorkout(int id);
        public Task RemoveWorkoutCascading(int id);
        public bool WorkoutExists(int id);
        public Task UpdateWorkoutSets(int id, List<int> setIds);
        public Task<IEnumerable<Workout>> GetAllContributedWorkouts();
        public Task<int> AccountAuthorized(int accountID);
    }
}
