﻿using MeFit_API.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;

namespace MeFit_API.Services.WorkoutService
{
    public class WorkoutService : IWorkoutService
    {
        private readonly MeFitDbContext _context;

        public WorkoutService(MeFitDbContext context)
        {
            _context = context;
        }

        public async Task<Workout> AddWorkout(Workout workout)
        {
            _context.Workouts.Add(workout);
            await _context.SaveChangesAsync();
            return workout;
        }

        public async Task<IEnumerable<Workout>> GetAllWorkouts()
        {
            return await _context.Workouts
               .Include(w => w.Sets)
               .ToListAsync();
        }

        public async Task<IEnumerable<Workout>> GetAllContributedWorkouts()
        {
            return await _context.Workouts
                .Include(w => w.Sets)
                .Where(w => w.MadeByContributor)
                .ToListAsync();
        }

        public async Task<Workout> GetWorkoutById(int id)
        {
            return await _context.Workouts.Where(w => w.Id == id).Include(w => w.Sets).FirstOrDefaultAsync();
        }

        public async Task RemoveWorkout(int id)
        {
            var workout = await _context.Workouts.FindAsync(id);
            _context.Workouts.Remove(workout);
            await _context.SaveChangesAsync();
        }

        public async Task RemoveWorkoutCascading(int id)
        {
            var workout = _context.Workouts.Include(g => g.Sets).Where(c => c.Id == id).First();

            foreach (var set in workout.Sets)
            {
                _context.Entry(set).State = EntityState.Deleted;
            }
            _context.Entry(workout).State = EntityState.Deleted;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateWorkout(Workout workout)
{
            _context.Entry(workout).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateWorkoutSets(int id, List<int> setIds)
        {
            Workout workoutToUpdate = await _context.Workouts
                .Include(a => a.Sets)
                .Where(a => a.Id == id)
                .FirstAsync();

            // Loop through Goals
            List<Set> sets = new();
            foreach (int set in setIds)
            {
                Set setToAdd = await _context.Sets.FindAsync(set);
                if (setToAdd == null)
                {
                    throw new KeyNotFoundException();
                }
                sets.Add(setToAdd);
            }

            workoutToUpdate.Sets = sets;
            await _context.SaveChangesAsync();
        }

        public bool WorkoutExists(int id)
        {
            return _context.Workouts.Any(e => e.Id == id);
        }

        public async Task<int> AccountAuthorized(int accountID)
        {
            Account account = await _context.Accounts.FindAsync(accountID);

            if (account.Admin == 3) { return 2; }
            if (account.Contributer == 3) { return 1; }
            return 0;
        }
    }
}
