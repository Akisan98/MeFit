﻿using MeFit_API.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeFit_API.Services.SetService
{
    public class SetService : ISetService
    {
        private readonly MeFitDbContext _context;

        public SetService(MeFitDbContext context)
        {
            _context = context;
        }

        public async Task<Set> AddSet(Set Set)
        {
            _context.Sets.Add(Set);
            await _context.SaveChangesAsync();
            return Set;
        }

        public async Task<IEnumerable<Set>> GetAllSets()
        {
            return await _context.Sets
              .Include(w => w.Exercise)
              .ToListAsync();
        }

        public async Task<Set> GetSetById(int id)
        {
            return await _context.Sets.Where(w => w.Id == id).Include(w => w.Exercise).FirstOrDefaultAsync();
        }

        public async Task RemoveSet(int id)
        {
            var set = await _context.Sets.FindAsync(id);
            _context.Sets.Remove(set);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateSet(Set Set)
        {
            _context.Entry(Set).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
        public bool SetExists(int id)
        {
            return _context.Sets.Any(e => e.Id == id);
        }
    }
}
