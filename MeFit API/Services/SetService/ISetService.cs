﻿using MeFit_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeFit_API.Services.SetService
{
    public interface ISetService
    {
        public Task<IEnumerable<Set>> GetAllSets();
        public Task<Set> GetSetById(int id);
        public Task<Set> AddSet(Set Set);
        public Task UpdateSet(Set Set);
        public Task RemoveSet(int id);
        public bool SetExists(int id);
    }
}
