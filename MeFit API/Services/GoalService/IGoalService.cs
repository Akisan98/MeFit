﻿using MeFit_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeFit_API.Services.GoalService
{
    public interface IGoalService
    {
        public Task<IEnumerable<Goal>> GetAllGoals();
        public Task<Goal> GetGoaleById(int id);
        public Task<Goal> AddGoal(Goal goal);
        public Task UpdateGoal(Goal goal);
        // public Task<IEnumerable<Profile>> UpdateProfileGoals(int id, List<Goal> goals);
        public Task RemoveGoal(int id);

        public Task RemoveGoalCascading(int id);
        public Task RemoveGoalAndWorkout(int id);
        public Task<IEnumerable<Goal>> GetAllGoalsById(int id);

        public Task UpdateGoalWorkouts(int goalId, List<int> workoutIds);

        public bool GoalExists(int id);
    }
}


