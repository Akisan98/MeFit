﻿using MeFit_API.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeFit_API.Services.GoalService
{
    public class GoalService : IGoalService
    {
        private readonly MeFitDbContext _context;

        public GoalService(MeFitDbContext context)
        {
            _context = context;
        }

        public async Task<Goal> AddGoal(Goal goal)
        {
            _context.Goals.Add(goal);
            await _context.SaveChangesAsync();
            return goal;
        }

        public async Task<IEnumerable<Goal>> GetAllGoals()
        {
            return await _context.Goals.Include(g => g.Workouts).ToListAsync();
            /*.Include(g => g.Account)*/
        }

        public async Task<IEnumerable<Goal>> GetAllGoalsById(int id)
        {
            return await _context.Goals.Include(g => g.Workouts).Where(c => c.AccountId == id).ToListAsync();
            /*.Include(g => g.Account)*/
        }
        public async Task<Goal> GetGoaleById(int id)
        {
            return await _context.Goals.FindAsync(id);
        }

        public async Task RemoveGoal(int id)
        {
            var goal = await _context.Goals.FindAsync(id);
            _context.Goals.Remove(goal);
            await _context.SaveChangesAsync();
        }

        public async Task RemoveGoalCascading(int id) 
        {
            var goal = _context.Goals.Include(g => g.Workouts).Where(c => c.Id == id).First();

            foreach (var workout in goal.Workouts)
            {
                var workout2 = _context.Workouts.Include(w => w.Sets).Where(c => c.Id == workout.Id).First();
                foreach (var set in workout2.Sets)
                {
                    _context.Entry(set).State = EntityState.Deleted;
                }
                _context.Entry(workout).State = EntityState.Deleted;
            }
            _context.Entry(goal).State = EntityState.Deleted;
            await _context.SaveChangesAsync();

            //_context.Goals.Remove(goal);
            //await _context.SaveChangesAsync();
        }

        public async Task RemoveGoalAndWorkout(int id)
        {
            var goal = _context.Goals.Include(g => g.Workouts).Where(c => c.Id == id).First();

            foreach (var workout in goal.Workouts)
            {
                var workout2 = _context.Workouts.Include(w => w.Sets).Where(c => c.Id == workout.Id).First();
                _context.Entry(workout).State = EntityState.Deleted;
            }
            _context.Entry(goal).State = EntityState.Deleted;
            await _context.SaveChangesAsync();
        }


        public async Task UpdateGoal(Goal goal)
        {
            _context.Entry(goal).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateGoalWorkouts(int goalId, List<int> workoutIds)
        {
            Goal goalToUpdate = await _context.Goals
                .Include(g => g.Workouts)
                .Where(c => c.Id == goalId)
                .FirstAsync();

            // Loop through Workouts
            List<Workout> workouts = new();
            foreach (int workout in workoutIds)
            {
                Workout workoutToAdd = await _context.Workouts.FindAsync(workout);
                if (workoutToAdd == null)
                {
                    throw new KeyNotFoundException();
                }
                workouts.Add(workoutToAdd);
            }

            goalToUpdate.Workouts = workouts;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAccountGoals(int accountId, List<int> goalIds)
        {
            Account accountToUpdate = await _context.Accounts
                .Include(a => a.Goals)
                .Where(a => a.Id == accountId)
                .FirstAsync();

            // Loop through Goals
            List<Goal> goals = new();
            foreach (int goal in goalIds)
            {
                Goal goalToAdd = await _context.Goals.FindAsync(goal);
                if (goalToAdd == null)
                {
                    throw new KeyNotFoundException();
                }
                goals.Add(goalToAdd);
            }

            accountToUpdate.Goals = goals;
            await _context.SaveChangesAsync();
        }

        public bool GoalExists(int id)
        {
            return _context.Goals.Any(e => e.Id == id);
        }
    }
}
