﻿using MeFit_API.Models;
using MeFit_API.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeFit_API.Services.ProgramService
{
    public interface IProgramService
    {
        public Task<IEnumerable<TrainingProgram>> GetAllPrograms();
        public Task<TrainingProgram> GetProgramById(int id);
        public Task<TrainingProgram> AddProgram(TrainingProgram program);
        public Task UpdateProgram(TrainingProgram program);
        // public Task<IEnumerable<Profile>> UpdateProfileGoals(int id, List<Goal> goals);
        public Task RemoveProgram(int id);
        public bool ProgramExists(int id);
        public Task UpdateProgramWorkouts(int id, List<int> workoutIds);

    }
}
