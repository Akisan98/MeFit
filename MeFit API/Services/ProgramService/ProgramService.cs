﻿using MeFit_API.Models;
using MeFit_API.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;

namespace MeFit_API.Services.ProgramService
{
    public class ProgramService : IProgramService
    {
        private readonly MeFitDbContext _context;

        public ProgramService(MeFitDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<TrainingProgram>> GetAllPrograms()
        {
            return await _context.Programs.Include(g => g.Workouts).ToListAsync();
        }
        public async Task<TrainingProgram> GetProgramById(int id)
        {
            return await _context.Programs.Where(a => a.Id == id).Include(a => a.Workouts).FirstOrDefaultAsync();
        }
        public async Task<TrainingProgram> AddProgram(TrainingProgram program)
        {
            _context.Programs.Add(program);
            await _context.SaveChangesAsync();
            return program;
        }
        public async Task UpdateProgram(TrainingProgram program)
        {
            _context.Entry(program).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
        public async Task RemoveProgram(int id)
        {
            var program = await _context.Programs.FindAsync(id);
            _context.Programs.Remove(program);
            await _context.SaveChangesAsync();
        }
        public bool ProgramExists(int id)
        {
            return _context.Programs.Any(e => e.Id == id);
        }

        public async Task UpdateProgramWorkouts(int id, List<int> workoutIds)
        {
            TrainingProgram programToUpdate = await _context.Programs
                .Include(a => a.Workouts)
                .Where(a => a.Id == id)
                .FirstAsync();

            // Loop through Goals
            List<Workout> workouts = new();
            foreach (int workout in workoutIds)
            {
                Workout workoutToAdd = await _context.Workouts.FindAsync(workout);
                if (workoutToAdd == null)
                {
                    throw new KeyNotFoundException();
                }
                workouts.Add(workoutToAdd);
            }

            programToUpdate.Workouts = workouts;
            await _context.SaveChangesAsync();
        }
    }
}
